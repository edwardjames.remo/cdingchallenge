package com.appetiser.codingchallenge

import android.content.Context
import io.realm.Realm
import io.realm.RealmConfiguration

object DataStore {
    val dataStore: Realm
        get() = Realm.getInstance(
            RealmConfiguration.Builder()
                .encryptionKey("super secret key that nobody knows a few more characters and I w".toByteArray())
                .build()
        )

    fun init(context: Context) {
        Realm.init(context)
    }
}

fun Realm.useForTransaction(block: (Realm) -> Unit) {
    use { realm -> realm.executeTransaction { block(it) } }
}