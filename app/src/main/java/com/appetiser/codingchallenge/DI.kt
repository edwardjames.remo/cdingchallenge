package com.appetiser.codingchallenge

import com.appetiser.codingchallenge.api.retrofitService
import com.appetiser.codingchallenge.itunes.api.ITunesApi
import com.appetiser.codingchallenge.itunessearch.api.ITunesSearchApi
import com.appetiser.codingchallenge.itunessearch.api.retrofit.ITunesSearchRetrofit
import com.appetiser.codingchallenge.itunessearch.api.retrofit.ITunesSearchService
import com.appetiser.codingchallenge.itunessearch.domain.usecase.GetRecentSearches
import com.appetiser.codingchallenge.itunessearch.domain.usecase.SearchInITunes
import com.appetiser.codingchallenge.itunessearch.domain.usecase.UpdateRecentSearches
import com.appetiser.codingchallenge.itunessearch.repo.RecentSearchRepo
import com.appetiser.codingchallenge.itunessearch.repo.realm.RecentSearchRealm
import com.appetiser.codingchallenge.itunessearch.ui.ITunesSearchPresenter
import com.appetiser.commons.mp.di.Provider
import com.facebook.stetho.okhttp3.StethoInterceptor
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import kotlinx.serialization.json.Json
import okhttp3.MediaType
import okhttp3.OkHttpClient


object DI {
    fun init() {
        Provider.register(singleton = true) {
            OkHttpClient.Builder().apply {
                if (BuildConfig.DEBUG) addNetworkInterceptor(StethoInterceptor())
            }.build()
        }

        // region iTunes Search
        Provider.register(singleton = true) {
            retrofitService<ITunesSearchService>(ITunesApi.baseUrl) {
                client(Provider.getInstance<OkHttpClient>())
                addConverterFactory(Json.nonstrict.asConverterFactory(MediaType.parse("application/json")!!))
            }
        }
        Provider.register(singleton = true) { ITunesSearchRetrofit() as ITunesSearchApi}
        Provider.register(singleton = true) { RecentSearchRealm() as RecentSearchRepo }
        Provider.register(singleton = true) { SearchInITunes() }
        Provider.register(singleton = true) { UpdateRecentSearches() }
        Provider.register(singleton = true) { GetRecentSearches() }
        Provider.register(singleton = true) { ITunesSearchPresenter() }
        // endregion
    }
}