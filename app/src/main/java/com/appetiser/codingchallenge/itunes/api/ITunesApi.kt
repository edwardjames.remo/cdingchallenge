package com.appetiser.codingchallenge.itunes.api

object ITunesApi {
    const val baseUrl = "https://itunes.apple.com/"
}