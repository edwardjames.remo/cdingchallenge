package com.appetiser.codingchallenge.itunes.domain.model

data class Track(
    val trackId: Long,
    val title: String,
    val artworkSmallUrl: String,
    val artWorkLargeUrl: String,
    val previewUrl: String,
    val price: Double,
    val currency: String,
    val genre: String,
    val description: String
)