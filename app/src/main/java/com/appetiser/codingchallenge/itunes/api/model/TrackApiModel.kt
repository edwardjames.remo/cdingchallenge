package com.appetiser.codingchallenge.itunes.api.model

import com.appetiser.codingchallenge.itunes.domain.model.Track
import kotlinx.serialization.Serializable

@Serializable
data class TrackApiModel(
    val trackId: Long,
    val trackName: String,
    val artworkUrl60: String,
    val artworkUrl100: String,
    val previewUrl: String = "",
    val trackPrice: Double = 0.0,
    val currency: String,
    val primaryGenreName: String,
    val longDescription: String
)

fun TrackApiModel.toDomain() =
    Track(
        trackId = trackId,
        title = trackName,
        artworkSmallUrl = artworkUrl60,
        artWorkLargeUrl = artworkUrl100,
        previewUrl = previewUrl,
        price = trackPrice,
        currency = currency,
        genre = primaryGenreName,
        description = longDescription
    )

fun List<TrackApiModel>.toDomain() = map { it.toDomain() }