package com.appetiser.codingchallenge

import android.app.Application
import com.appetiser.commons.mp.logging.DefaultLogger
import com.appetiser.commons.mp.logging.Log
import com.appetiser.commons.mp.logging.LogLevel
import com.facebook.stetho.Stetho

class CodingChallengeApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Log.init(LogLevel.DEBUG)
            Log.registerLogger(DefaultLogger())
        } else {
            Log.init(LogLevel.INFO)
        }

        Stetho.initializeWithDefaults(this)
        DataStore.init(this)
        DI.init()
    }
}