package com.appetiser.codingchallenge.api

import retrofit2.Retrofit
import retrofit2.create

inline fun <reified T> retrofitService(baseUrl: String, init: Retrofit.Builder.() -> Unit = {}): T {
    val builder = Retrofit.Builder().baseUrl(baseUrl)
    builder.init()
    return builder.build().create()
}