package com.appetiser.codingchallenge.api

class ApiFailure(
    val errorCode: Int,
    message: String,
    val errorBody: String = ""
) : Exception(message)