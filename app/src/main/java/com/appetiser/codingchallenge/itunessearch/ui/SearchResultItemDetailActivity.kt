package com.appetiser.codingchallenge.itunessearch.ui

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.appetiser.codingchallenge.R
import com.appetiser.commons.mp.di.Injection
import kotlinx.android.synthetic.main.activity_searchresultitem_detail.*

/**
 * An activity representing a single SearchResultItem detail screen. This
 * activity is only used on narrow width devices. On tablet-size devices,
 * item details are presented side-by-side with a list of items
 * in a [SearchResultItemListActivity].
 */
class SearchResultItemDetailActivity : AppCompatActivity() {
    private val trackId get() = intent.getLongExtra(SearchResultItemDetailFragment.ARG_TRACK_ID, -1)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_searchresultitem_detail)
        setSupportActionBar(detail_toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        if (savedInstanceState == null) {
            val fragment = SearchResultItemDetailFragment().apply {
                arguments = Bundle().apply {
                    putLong(SearchResultItemDetailFragment.ARG_TRACK_ID, trackId)
                }
            }

            supportFragmentManager.beginTransaction()
                .add(R.id.searchresultitem_detail_container, fragment)
                .commit()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem) =
        when (item.itemId) {
            android.R.id.home -> {

                // This ID represents the Home or Up button. In the case of this
                // activity, the Up button is shown. For
                // more details, see the Navigation pattern on Android Design:
                //
                // http://developer.android.com/design/patterns/navigation.html#up-vs-back

                navigateUpTo(Intent(this, SearchResultItemListActivity::class.java))

                true
            }
            else -> super.onOptionsItemSelected(item)
        }
}