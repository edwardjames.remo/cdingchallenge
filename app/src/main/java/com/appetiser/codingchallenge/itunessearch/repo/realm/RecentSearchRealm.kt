package com.appetiser.codingchallenge.itunessearch.repo.realm

import com.appetiser.codingchallenge.DataStore
import com.appetiser.codingchallenge.itunessearch.domain.model.RecentSearch
import com.appetiser.codingchallenge.itunessearch.repo.RecentSearchRepo
import com.appetiser.codingchallenge.itunessearch.repo.model.RecentSearchEntity
import com.appetiser.codingchallenge.itunessearch.repo.model.toDomain
import com.appetiser.codingchallenge.itunessearch.repo.model.toEntity
import com.appetiser.codingchallenge.useForTransaction
import io.realm.kotlin.where
import java.util.*

class RecentSearchRealm : RecentSearchRepo {

    override suspend fun save(recentSearch: RecentSearch): RecentSearch {
        val entity = recentSearch.toEntity()
        if (entity.id == null) entity.id = UUID.randomUUID().toString()
        DataStore.dataStore.useForTransaction { it.insertOrUpdate(entity) }
        return entity.toDomain()
    }

    override suspend fun getAll(): List<RecentSearch> {
        return DataStore.dataStore.use { it.where<RecentSearchEntity>().findAll().toDomain() }
    }

    override suspend fun deleteAll(recentSearches: List<RecentSearch>) {
        DataStore.dataStore.useForTransaction { realm ->
            realm.where<RecentSearchEntity>().`in`(
                RecentSearchEntity::id.name,
                recentSearches.map { it.id }.toTypedArray()
            ).findAll().deleteAllFromRealm()
        }
    }

    override suspend fun findByTerm(term: String): List<RecentSearch> {
        return DataStore.dataStore.use {
            it.where<RecentSearchEntity>()
                .equalTo(RecentSearchEntity::term.name, term)
                .findAll()
                .toDomain()
        }
    }
}