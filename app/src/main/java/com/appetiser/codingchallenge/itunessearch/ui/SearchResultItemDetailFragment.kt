package com.appetiser.codingchallenge.itunessearch.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.appetiser.codingchallenge.R
import com.appetiser.codingchallenge.itunessearch.ui.model.TrackUiModel
import com.appetiser.commons.mp.di.Injection
import kotlinx.android.synthetic.main.activity_searchresultitem_detail.*
import kotlinx.android.synthetic.main.searchresultitem_detail.view.*

/**
 * A fragment representing a single SearchResultItem detail screen.
 * This fragment is either contained in a [SearchResultItemListActivity]
 * in two-pane mode (on tablets) or a [SearchResultItemDetailActivity]
 * on handsets.
 */
class SearchResultItemDetailFragment : Fragment() {
    private val iTunesSearchPresenter: ITunesSearchPresenter by Injection()
    private var item: TrackUiModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let { bundle ->
            if (bundle.containsKey(ARG_TRACK_ID)) {
                item = iTunesSearchPresenter.results.find { it.trackId == bundle.getLong(ARG_TRACK_ID) }
                activity?.toolbar_layout?.title = item?.title
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.searchresultitem_detail, container, false)

        val item = item ?: return rootView

        rootView?.apply {
            searchResultItemDescription.text = item.description
            item.genre.let { genre ->
                searchResultItemGenre.text = "Genre: $genre"
            }
            searchResultItemBuyBtn.text = item.buyBtnText
        }


        return rootView
    }

    companion object {
        /**
         * The track id of the result selected from the list
         */
        const val ARG_TRACK_ID = "track_id"
    }
}