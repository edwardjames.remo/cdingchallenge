package com.appetiser.codingchallenge.itunessearch.api.model

import com.appetiser.codingchallenge.itunes.api.model.TrackApiModel
import kotlinx.serialization.Serializable

@Serializable
data class TrackApiModel(
    val resultCount: Int,
    val results: List<TrackApiModel>
)