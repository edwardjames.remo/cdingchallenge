package com.appetiser.codingchallenge.itunessearch.api.retrofit

import com.appetiser.codingchallenge.api.ApiFailure
import com.appetiser.commons.mp.di.Injection
import com.appetiser.codingchallenge.itunessearch.api.model.TrackApiModel
import com.appetiser.codingchallenge.itunes.api.model.toDomain
import com.appetiser.codingchallenge.itunessearch.domain.model.SearchFilter
import com.appetiser.codingchallenge.itunes.domain.model.Track
import com.appetiser.codingchallenge.itunessearch.api.ITunesSearchApi
import kotlin.coroutines.suspendCoroutine
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.coroutines.resumeWithException

/**
 * Executes requests to iTunes service
 */
class ITunesSearchRetrofit:
    ITunesSearchApi {
    private val searchApi: ITunesSearchService by Injection()

    /**
     * Performs a search in iTunes for the specified [filter]
     */
    override suspend fun query(filter: SearchFilter): List<Track> = suspendCoroutine { cont ->
        searchApi.search(filter.term, filter.country, filter.media.strValue)
            .enqueue(object : Callback<TrackApiModel> {
                override fun onFailure(call: Call<TrackApiModel>, t: Throwable) {
                    cont.resumeWithException(t)
                }

                override fun onResponse(
                    call: Call<TrackApiModel>,
                    response: Response<TrackApiModel>
                ) {
                    if (response.isSuccessful) {
                        cont.resumeWith(
                            Result.success(response.body()?.results?.toDomain().orEmpty())
                        )
                    } else {
                        cont.resumeWithException(
                            ApiFailure(
                                response.code(),
                                response.message(),
                                response.errorBody()?.string().orEmpty()
                            )
                        )
                    }
                }
            })
    }
}