package com.appetiser.codingchallenge.itunessearch.ui.model

import com.appetiser.codingchallenge.itunes.domain.model.Track

data class TrackUiModel(
    val trackId: Long,
    val title: String,
    val artworkSmallUrl: String,
    val artWorkLargeUrl: String,
    val previewUrl: String,
    val price: Double,
    val currency: String,
    val genre: String,
    val description: String
)

fun Track.toUiModel() = TrackUiModel(
    trackId = trackId,
    title = title,
    artworkSmallUrl = artworkSmallUrl,
    artWorkLargeUrl = artWorkLargeUrl,
    previewUrl = previewUrl,
    price = price,
    currency = currency,
    genre = genre,
    description = description
)

fun List<Track>.toUiModel() = map { it.toUiModel() }