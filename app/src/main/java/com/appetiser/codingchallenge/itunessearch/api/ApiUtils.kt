package com.appetiser.codingchallenge.itunessearch.api

import com.appetiser.codingchallenge.itunes.api.ITunesApi

object ITunesSearchUrl {
    const val path = "search"
    const val queryTerm = "term"
    const val queryCountry = "country"
    const val queryMedia = "media"
}

val ITunesApi.search get() = ITunesSearchUrl