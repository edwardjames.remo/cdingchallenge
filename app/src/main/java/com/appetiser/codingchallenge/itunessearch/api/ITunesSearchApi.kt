package com.appetiser.codingchallenge.itunessearch.api

import com.appetiser.codingchallenge.itunes.domain.model.Track
import com.appetiser.codingchallenge.itunessearch.domain.model.SearchFilter

interface ITunesSearchApi {
    suspend fun query(filter: SearchFilter): List<Track>
}