package com.appetiser.codingchallenge.itunessearch.domain.usecase

import com.appetiser.codingchallenge.itunes.domain.model.Track
import com.appetiser.codingchallenge.itunessearch.api.ITunesSearchApi
import com.appetiser.codingchallenge.itunessearch.domain.model.SearchFilter
import com.appetiser.commons.mp.async.Storage
import com.appetiser.commons.mp.di.Injection
import com.appetiser.commons.mp.domain.UseCase
import kotlinx.coroutines.Dispatchers

class SearchInITunes : UseCase<SearchFilter, List<Track>>() {
    override val dispatcher = Dispatchers.Storage

    private val iTunesSearchApi: ITunesSearchApi by Injection()

    override suspend fun executeSuspending(request: SearchFilter): List<Track> {
        return iTunesSearchApi.query(request)
    }
}