package com.appetiser.codingchallenge.itunessearch.ui

import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.EditorInfo
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.appetiser.codingchallenge.R
import com.appetiser.codingchallenge.itunessearch.ui.model.TrackUiModel
import com.appetiser.commons.droid.ui.isDisplayed
import com.appetiser.commons.mp.di.Injection
import com.appetiser.commons.mp.utils.addHandler
import kotlinx.android.synthetic.main.activity_searchresultitem_list.*
import kotlinx.android.synthetic.main.searchresultitem_list.*


/**
 * An activity representing a list of Pings. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a [SearchResultItemDetailActivity] representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
class SearchResultItemListActivity : AppCompatActivity() {

    private val iTunesSearchPresenter: ITunesSearchPresenter by Injection()
    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private var twoPane: Boolean = false
    private val adapter by lazy { SearchResultItemsAdapter(this, twoPane) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_searchresultitem_list)

        rootLayout.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN)

        window.setFlags(
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        )

        if (searchresultitem_detail_container != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            twoPane = true
        }

        setupRecyclerView(searchresultitem_list)

        iTunesSearchPresenter.event.addHandler(this) {
            when (it) {
                is ITunesSearchEvent.LoadSearchResults -> onLoadSearchResults(it.results)
                is ITunesSearchEvent.ShowRecentSearches -> showRecentSearches(it.recentSearches)
                is ITunesSearchEvent.HideRecentSearches -> hideRecentSearches()
            }
        }

        searchInput.setOnClickListener {
            iTunesSearchPresenter.onSearchActivated()
        }

        searchInput?.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                iTunesSearchPresenter.search(term = searchInput.text.toString())
                true
            } else false
        }

    }

    private fun hideRecentSearches() {
        recentSearchList.isDisplayed = false
    }

    private fun showRecentSearches(recentSearches: List<String>) {
        recentSearchList.adapter = ArrayAdapter<String>(
            this,
            R.layout.recent_search_item,
            R.id.recentSearchTerm,
            recentSearches
        )
        recentSearchList.isDisplayed = true

        recentSearchList.setOnItemClickListener { _, _, position, _ ->
            iTunesSearchPresenter.search(recentSearches[position])
        }
    }

    private fun onLoadSearchResults(results: List<TrackUiModel>) {
        adapter.items = results
    }

    override fun onStart() {
        super.onStart()
        iTunesSearchPresenter.start()
    }

    private fun setupRecyclerView(recyclerView: RecyclerView) {
        recyclerView.adapter = adapter
    }

}