package com.appetiser.codingchallenge.itunessearch.domain.usecase

import com.appetiser.codingchallenge.itunessearch.domain.model.RecentSearch
import com.appetiser.codingchallenge.itunessearch.repo.RecentSearchRepo
import com.appetiser.commons.mp.async.Storage
import com.appetiser.commons.mp.di.Injection
import com.appetiser.commons.mp.domain.UseCase
import kotlinx.coroutines.Dispatchers
import java.util.*

/**
 * Adds the [Request.term] to the recent searches if it does not yet exist or
 * updates the [RecentSearch.lastSearchDate] if it has been previously saved.
 * Also deletes other [RecentSearch]es aside from the 5 newest based on
 * [RecentSearch.lastSearchDate]
 */
class UpdateRecentSearches : UseCase<UpdateRecentSearches.Request, Unit>() {
    override val dispatcher = Dispatchers.Storage

    private val recentSearchRepo: RecentSearchRepo by Injection()

    override suspend fun executeSuspending(request: Request) {
        val existingSearch = recentSearchRepo.findByTerm(request.term).firstOrNull()

        val newSearch = RecentSearch(term = request.term, lastSearchDate = Date())

        if (existingSearch == null) {
            recentSearchRepo.save(newSearch)
        } else {
            existingSearch.lastSearchDate = Date()
            recentSearchRepo.save(existingSearch)
        }

        recentSearchRepo.deleteAll(
            recentSearchRepo.getAll().asSequence()
                .sortedByDescending { it.lastSearchDate }
                .drop(5).toList()
        )

    }

    class Request(val term: String)
}