package com.appetiser.codingchallenge.itunessearch.domain.model

/**
 * A representation of the search to perform.
 *
 * @param term text string you want to search for.
 * @param country two-letter country code for the store you want to search.
 * @param media media type you want to search for.
 *
 * @property term text string you want to search for.
 * @property country two-letter country code for the store you want to search.
 * @property media media type you want to search for.
 *
 */
data class SearchFilter(
    val term: String = defaultTerm,
    val country: String = defaultCountry,
    val media: SearchMediaType = defaultMedia
) {
    companion object {
        const val defaultTerm = "star"
        const val defaultCountry = "au"
        val defaultMedia = SearchMediaType.MOVIE
    }
}