package com.appetiser.codingchallenge.itunessearch.repo.model

import com.appetiser.codingchallenge.itunessearch.domain.model.RecentSearch
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import java.util.*

open class RecentSearchEntity(
    @PrimaryKey var id: String? = null,
    var term: String = "",
    var lastSearchDate: Date = Date()
): RealmObject()

fun RecentSearch.toEntity() = RecentSearchEntity(
    id = id,
    term = term,
    lastSearchDate = lastSearchDate
)

fun RecentSearchEntity.toDomain() = RecentSearch(
    id = id,
    term = term,
    lastSearchDate = lastSearchDate
)

fun List<RecentSearchEntity>.toDomain() = map { it.toDomain() }