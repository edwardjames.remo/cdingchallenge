package com.appetiser.codingchallenge.itunessearch.ui

import com.appetiser.codingchallenge.itunessearch.ui.model.TrackUiModel
import com.appetiser.commons.mp.utils.Event

sealed class ITunesSearchEvent : Event {
    class LoadSearchResults(val results: List<TrackUiModel>) : ITunesSearchEvent()
    class ShowRecentSearches(val recentSearches: List<String>): ITunesSearchEvent()
    object HideRecentSearches: ITunesSearchEvent()
}