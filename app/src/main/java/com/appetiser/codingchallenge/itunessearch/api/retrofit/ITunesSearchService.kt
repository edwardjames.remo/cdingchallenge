package com.appetiser.codingchallenge.itunessearch.api.retrofit

import com.appetiser.codingchallenge.itunes.api.ITunesApi
import com.appetiser.codingchallenge.itunessearch.api.model.TrackApiModel
import com.appetiser.codingchallenge.itunessearch.api.search
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ITunesSearchService {
    @GET(ITunesApi.search.path)
    fun search(
        @Query(ITunesApi.search.queryTerm) term: String,
        @Query(ITunesApi.search.queryCountry) country: String,
        @Query(ITunesApi.search.queryMedia) media: String
    ): Call<TrackApiModel>
}