package com.appetiser.codingchallenge.itunessearch.domain.usecase

import com.appetiser.codingchallenge.itunessearch.domain.model.RecentSearch
import com.appetiser.codingchallenge.itunessearch.repo.RecentSearchRepo
import com.appetiser.commons.mp.async.Storage
import com.appetiser.commons.mp.di.Injection
import com.appetiser.commons.mp.domain.UseCaseNoPayload
import kotlinx.coroutines.Dispatchers

class GetRecentSearches : UseCaseNoPayload<GetRecentSearches.Response>() {
    override val dispatcher = Dispatchers.Storage

    private val recentSearchRepo: RecentSearchRepo by Injection()

    override suspend fun executeSuspending(request: Unit): Response {
        return Response(recentSearchRepo.getAll().sortedByDescending { it.lastSearchDate })
    }

    class Response(val recentSearches: List<RecentSearch>)
}