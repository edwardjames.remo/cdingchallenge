package com.appetiser.codingchallenge.itunessearch.repo

import com.appetiser.codingchallenge.itunessearch.domain.model.RecentSearch

interface RecentSearchRepo {
    suspend fun save(recentSearch: RecentSearch): RecentSearch
    suspend fun getAll(): List<RecentSearch>
    suspend fun deleteAll(recentSearches: List<RecentSearch>)
    suspend fun findByTerm(term: String): List<RecentSearch>
}