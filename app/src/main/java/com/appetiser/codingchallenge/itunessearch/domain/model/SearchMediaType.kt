package com.appetiser.codingchallenge.itunessearch.domain.model

enum class SearchMediaType (val strValue: String) {
    MOVIE("movie"),
    PODCAST("podcast"),
    MUSIC("music"),
    MUSIC_VIDEO("musicVideo"),
    AUDIO_BOOK("audiobook"),
    SHORT_FILM("shortFilm"),
    TV_SHOW("tvShow"),
    SOFTWARE("software"),
    EBOOK("ebook"),
    ALL("all")
}