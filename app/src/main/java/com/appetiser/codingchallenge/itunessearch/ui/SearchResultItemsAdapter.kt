package com.appetiser.codingchallenge.itunessearch.ui

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.appetiser.codingchallenge.R
import com.appetiser.codingchallenge.itunessearch.ui.model.TrackUiModel
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.searchresultitem_list_content.view.*

class SearchResultItemsAdapter(
    private val parentActivity: SearchResultItemListActivity,
    private val twoPane: Boolean
) : RecyclerView.Adapter<SearchResultItemsAdapter.ViewHolder>() {

    private val onClickListener: View.OnClickListener
    var items = listOf<TrackUiModel>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    init {
        onClickListener = View.OnClickListener { v ->
            val item = v.tag as TrackUiModel
            if (twoPane) {
                val fragment = SearchResultItemDetailFragment()
                    .apply {
                    arguments = Bundle().apply {
                        putLong(SearchResultItemDetailFragment.ARG_TRACK_ID, item.trackId)
                    }
                }
                parentActivity.supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.searchresultitem_detail_container, fragment)
                    .commit()
            } else {
                val intent =
                    Intent(
                        v.context,
                        SearchResultItemDetailActivity::class.java
                    ).apply {
                        putExtra(SearchResultItemDetailFragment.ARG_TRACK_ID, item.trackId)
                    }
                v.context.startActivity(intent)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.searchresultitem_list_content, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        holder.itemView.searchResultItemTitle.text = item.title
        holder.itemView.searchResultItemGenre.text = item.genre
        holder.itemView.searchResultItemBuyBtn.text = item.buyBtnText

        Glide.with(holder.itemView)
            .load(item.artWorkLargeUrl)
            .into(holder.itemView.searchResultItemArtwork)

        with(holder.itemView) {
            tag = item
            setOnClickListener(onClickListener)
        }
    }

    override fun getItemCount() = items.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view)
}