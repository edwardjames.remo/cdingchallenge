package com.appetiser.codingchallenge.itunessearch.domain.model

import java.util.*

data class RecentSearch(
    val id: String? = null,
    val term: String,
    var lastSearchDate: Date = Date()
)