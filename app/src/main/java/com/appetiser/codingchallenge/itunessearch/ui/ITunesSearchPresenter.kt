package com.appetiser.codingchallenge.itunessearch.ui

import com.appetiser.codingchallenge.itunes.domain.model.Track
import com.appetiser.codingchallenge.itunessearch.api.ITunesSearchUrl
import com.appetiser.codingchallenge.itunessearch.domain.model.SearchFilter
import com.appetiser.codingchallenge.itunessearch.domain.model.SearchMediaType
import com.appetiser.codingchallenge.itunessearch.domain.usecase.GetRecentSearches
import com.appetiser.codingchallenge.itunessearch.domain.usecase.SearchInITunes
import com.appetiser.codingchallenge.itunessearch.domain.usecase.UpdateRecentSearches
import com.appetiser.codingchallenge.itunessearch.ui.model.TrackUiModel
import com.appetiser.codingchallenge.itunessearch.ui.model.toUiModel
import com.appetiser.commons.mp.di.Injection
import com.appetiser.commons.mp.domain.executeSuspending
import com.appetiser.commons.mp.ui.Presenter
import com.appetiser.commons.mp.utils.Observable
import kotlinx.coroutines.launch

class ITunesSearchPresenter : Presenter() {
    private val searchInITunes: SearchInITunes by Injection()
    private val updateRecentSearches: UpdateRecentSearches by Injection()
    private val getRecentSearches: GetRecentSearches by Injection()

    val event = Observable<ITunesSearchEvent>()
    var results = listOf<TrackUiModel>()
        private set

    fun start() = launch(coroutineContext) {
        search()
    }

    fun onSearchActivated() = launch(coroutineContext) {
        event.value = ITunesSearchEvent.ShowRecentSearches(
            getRecentSearches.executeSuspending().recentSearches.map { it.term }
        )
    }

    fun search(
        term: String? = null,
        country: String? = SearchFilter.defaultCountry,
        mediaType: SearchMediaType? = SearchFilter.defaultMedia
    ) = launch(coroutineContext) {
        val searchTerm = term ?: SearchFilter.defaultTerm

        val searchResults = try {
            searchInITunes.executeSuspending(
                SearchFilter(
                    searchTerm,
                    country ?: SearchFilter.defaultCountry,
                    mediaType ?: SearchFilter.defaultMedia
                )
            )
        } catch (e: Exception) {
            listOf<Track>()
        }

        if (term != null) {
            updateRecentSearches.executeSuspending(
                UpdateRecentSearches.Request(searchTerm)
            )
        }

        results = searchResults.toUiModel()
        event.value = ITunesSearchEvent.LoadSearchResults(results)
        event.value = ITunesSearchEvent.HideRecentSearches
    }
}