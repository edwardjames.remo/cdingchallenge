package com.appetiser.codingchallenge.itunessearch.ui

import com.appetiser.codingchallenge.itunessearch.ui.model.TrackUiModel

val TrackUiModel.buyBtnText get() =
    if (price == 0.0) "Free" else "BUY $currency $price"