package com.appetiser.commons.mp.utils

/**
 * Kind of like String.format() except that arguments are named.
 *
 * For example formatting a string `"...{str}..."` with a map `["str": "longer string"]`
 * will result to `"...longer string..."`.
 *
 * This returns a new [String] instance and does not affect the string it is applied on
 */
fun String.namedFormat(args: Map<String, *>): String {
    var newStr = this.substring(0)

    for ((key, value) in args) {
        newStr = newStr.replace("{$key}", value.toString())
    }

    return newStr
}


/**
 * Kind of like String.format() except that arguments are named.
 *
 * For example formatting a string `"...{str}..."` with a map `["str": "longer string"]`
 * will result to `"...longer string..."`.
 *
 * This returns a new [String] instance and does not affect the string it is applied on
 */
fun String.namedFormat(vararg args: Pair<String, *>): String {
    var newStr = this.substring(0)

    for ((key, value) in args) {
        newStr = newStr.replace("{$key}", value.toString())
    }

    return newStr
}

val Regex.Companion.whiteSpace get() = Regex("""\s""")
