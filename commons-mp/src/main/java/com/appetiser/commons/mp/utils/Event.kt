package com.appetiser.commons.mp.utils

/**
 * Used to mark a `sealed class` that defines [Event]s to be consumed
 * by an event handler.
 */
interface Event

/**
 * Used to get notifications to changes on an [Observable] [Event]. The
 * main difference with adding an [Observer] is that the `Observable Event`
 * is consumed and set to `null` after notifying the [handler] and that
 * the [handler] is not notified if the Observable Event is set to `null`.
 */
fun <T : Event> Observable<T>.addHandler(key: Any, handler: (T) -> Unit) {
    // observe event value
    addObserver(key) {
        if (it != null) {
            // dispatch the event to the handler
            handler(it)
            // consume the event
            value = null
        }
    }
}

/**
 * Removes the handler added with the [key] from the list of handlers
 * that receives [Event] notifications.
 */
fun <T : Event> Observable<T>.removeHandler(key: Any) {
    removeObserver(key)
}

fun <T : Event> Observable<T>.post(event: T) {
    value = event
}