package com.appetiser.commons.mp.domain

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

abstract class UseCase<Req, Resp> : CoroutineScope {
    private val job = Job()
    protected abstract val dispatcher: CoroutineDispatcher
    override val coroutineContext: CoroutineContext
        get() = dispatcher + job

    /**
     * Executes the [UseCase] asynchronously and listens for the result.
     */
    open fun execute(
        request: Req,
        onSuccess: (Resp) -> Unit = {},
        onProgress: (progress: Int) -> Unit = {},
        onFail: (Throwable) -> Unit = {}
    ) {
        launch(coroutineContext) {
            try {
                onSuccess(executeSuspending(request))
            } catch (error: Throwable) {
                onFail(error)
            }
        }
    }

    /**
     * Executes the [UseCase] and waits for the result.
     */
    open suspend fun executeSuspending(request: Req): Resp = suspendCoroutine { cont ->
        execute(
            request,
            onSuccess = { cont.resume(it) },
            onFail = { cont.resumeWithException(it) }
        )
    }
}

typealias UseCaseNoPayload<Resp> = UseCase<Unit, Resp>

suspend fun <Resp> UseCaseNoPayload<Resp>.executeSuspending() = executeSuspending(Unit)
fun <Resp> UseCaseNoPayload<Resp>.execute(
    onSuccess: (Resp) -> Unit = {},
    onProgress: (progress: Int) -> Unit = {},
    onFail: (Throwable) -> Unit = {}
) = execute(Unit, onSuccess, onProgress, onFail)