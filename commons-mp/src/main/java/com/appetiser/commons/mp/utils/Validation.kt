package com.appetiser.commons.mp.utils

interface Validator<T> {
    fun validate(item: T): ValidationResult
    class Error(
        val field: String,
        val messageKey: String,
        val format: String? = null,
        val minLength: Int? = null,
        val maxLength: Int? = null
    )
}

typealias ValidationResult = MutableList<Validator.Error>

fun validationResult(init: ValidationResult.() -> Unit) = mutableListOf<Validator.Error>().apply { init() }
fun List<Validator.Error>.toValidationResult(): ValidationResult = toMutableList()

val List<Validator.Error>.isValid get() = isEmpty()
