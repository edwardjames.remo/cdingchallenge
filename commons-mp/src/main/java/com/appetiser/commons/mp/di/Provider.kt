package com.appetiser.commons.mp.di

import kotlin.reflect.KProperty

/**
 * Keeps track of registered implementations for all Types
 */
class Provider<T>
@PublishedApi
internal constructor(val create: () -> T) {
    fun instance(): T = create()

    companion object Factory {
        @PublishedApi
        internal val providerMap = mutableMapOf<InstanceIdentifier, Provider<*>>()

        @PublishedApi
        internal val singletonMap = mutableMapOf<InstanceIdentifier, Any>()

        /**
         * Registers an instance creator as a provider. A unique [id] can be supplied for multiple
         * creators for the same type. If multiple creators with the same id (or if no ids were supplied)
         * are registered, the last creator will always be used.
         *
         * ### Usage
         *     interface A
         *     class B : A
         *     class C : A
         *     Provider.register { arg -> B() as A }
         *     Provider.register(id = "uniqueID") { arg -> C() as A }
         * @param id unique identifier for the creator being registered. Can be null.
         * @param instanceCreator a lambda expression that will create an instance of the requested type [T].
         * @param T the type associated to the creator being registered.
         * @since 0.0.1
         */
        inline fun <reified T: Any> register(id: Any? = null, singleton: Boolean = false, noinline instanceCreator: () -> T) {
            val identifier = InstanceIdentifier(T::class, id)

            if (singleton) {
                // store an only instance created by the instance creator
                singletonMap[identifier] = instanceCreator.invoke()
                // use a provider that gets an instance from the singleton instances
                providerMap[identifier] = Provider { singletonMap[identifier] }
            } else {
                // just use the provided instance creator
                providerMap[identifier] = Provider(instanceCreator)
            }
        }

        inline fun <reified T> getInstance(id: Any? = null): T {
            val identifier = InstanceIdentifier(T::class, id)

            val provider =
                providerMap[identifier] ?: throw NoProviderFoundException("No provider found for ${T::class}")

            val instance = try {
                provider.instance()
            } catch (e: Throwable) {
                throw InstanceCreationFailedException("instance creation failed for ${T::class}", e)
            }

            return instance as T ?: throw InstanceCreationFailedException("provider returned null for ${T::class}")
        }
    }

}

/**
 * A delegate for injecting instances to properties
 */
class Injection(val id: Any? = null) {
    inline operator fun <reified T> getValue(thisRef: Any?, property: KProperty<*>): T {
        return Provider.getInstance(id)
    }
}

@PublishedApi
internal data class InstanceIdentifier(val type: Any, val id: Any?)

class NoProviderFoundException(msg: String) : Exception(msg)
class InstanceCreationFailedException(msg: String, cause: Throwable? = null) : Exception(msg, cause)