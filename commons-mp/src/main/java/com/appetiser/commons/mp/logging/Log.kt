package com.appetiser.commons.mp.logging

object Log : Logger {
    private val loggers = mutableSetOf<Logger>()
    private val blackList = mutableMapOf<String, LogLevel>()
    var currentLevel = LogLevel.default
        private set

    fun registerLogger(logger: Logger) {
        loggers.add(logger)
    }

    override fun d(tag: String, msg: String, throwable: Throwable?) {
        log(LogLevel.DEBUG, tag, msg, throwable)
    }

    override fun i(tag: String, msg: String, throwable: Throwable?) {
        log(LogLevel.INFO, tag, msg, throwable)
    }

    override fun w(tag: String, msg: String, throwable: Throwable?) {
        log(LogLevel.WARNING, tag, msg, throwable)
    }

    override fun e(tag: String, msg: String, throwable: Throwable?) {
        log(LogLevel.ERROR, tag, msg, throwable)
    }

    override fun wtf(tag: String, msg: String, throwable: Throwable?) {
        log(LogLevel.WTF, tag, msg, throwable)
    }

    fun init(level: LogLevel) {
        currentLevel = level
    }

    /**
     * Prevents logging of [tag]s for the specified [logLevel] or lower.
     * If no [LogLevel] is specified it is set to the highest log level,
     * [LogLevel.WTF], meaning all logs are blocked
     */
    fun blackList(tag: String, logLevel: LogLevel = LogLevel.WTF) {
        blackList[tag] = logLevel
    }

    private fun log(logLevel: LogLevel, tag: String, msg: String, throwable: Throwable? = null) {
        if (logLevel < currentLevel) return

        val blackListEntry = blackList[tag]
        if (blackListEntry != null && logLevel <= blackListEntry) return

        when (logLevel) {
            LogLevel.DEBUG -> loggers.forEach { it.d(tag, msg, throwable) }
            LogLevel.INFO -> loggers.forEach { it.i(tag, msg, throwable) }
            LogLevel.WARNING -> loggers.forEach { it.w(tag, msg, throwable) }
            LogLevel.ERROR -> loggers.forEach { it.e(tag, msg, throwable) }
            LogLevel.WTF -> loggers.forEach { it.wtf(tag, msg, throwable) }
        }
    }
}