package com.appetiser.commons.mp.async

import kotlinx.coroutines.Dispatchers

val Dispatchers.Storage get() = IO