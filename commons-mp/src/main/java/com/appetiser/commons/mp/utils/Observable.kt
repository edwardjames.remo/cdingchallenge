package com.appetiser.commons.mp.utils

typealias Observer<T> = (T?) -> Unit

class Observable<T>(private val initial: T? = null) {
    private val observers = mutableMapOf<Any, Observer<T>>()
    val isDirty get() = value != initial

    var value: T? = initial
        set(value) {
            // don't do anything if there will be no changes to the value
            if (field == value) return

            field = value

            // notify observers
            observers.forEach { it.value.invoke(value) }
        }

    /**
     * Adds an [Observer] ot notify of changes to the [value] contained in this
     * [Observable]. There is no guarantee that getting [value] will return the
     * same instance that caused the [observer] to be notified, hence you should
     * only use the value passed to the [observer].
     */
    fun addObserver(key: Any, observer: Observer<T>) {
        observers[key] = observer
    }

    /**
     * Removes the [Observer] added with the corresponding [key] from
     * the list of [Observer] that gets notified of changes to [value].
     */
    fun removeObserver(key: Any) {
        observers.remove(key)
    }

    /**
     * removes the specified [observer] from the list of [Observer]
     * that gets notified of changes to [value].
     */
    fun removeObserver(observer: Observer<T>) {
        val key = observers.entries.find { (_, value) -> value == observer }?.key ?: return
        removeObserver(key)
    }

    override fun toString(): String = value.toString()

    override fun equals(other: Any?): Boolean {
        return other is Observable<*> && other.value == value
    }

    override fun hashCode(): Int {
        return value?.hashCode() ?: 0
    }
}