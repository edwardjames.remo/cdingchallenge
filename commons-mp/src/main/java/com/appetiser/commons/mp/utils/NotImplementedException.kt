package com.appetiser.commons.mp.utils

class NotImplementedException(msg: String? = null) : Exception(msg)