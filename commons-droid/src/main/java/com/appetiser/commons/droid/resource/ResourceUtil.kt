package com.appetiser.commons.droid.resource

import android.content.Context

const val RES_STRING = "string"
const val RES_DRAWABLE = "drawable"


fun Context.getDrawableResId(name: String): Int = getResId(RES_DRAWABLE, name)

fun Context.getStringResId(name: String): Int = getResId(RES_STRING, name)
fun Context.getString(name: String): String? = try {
    getString(getStringResId(name))
} catch (e: Exception) {
    null
}

fun Context.getResId(resType: String, name: String): Int = try {
    resources.getIdentifier(name, resType, packageName)
} catch (e: Throwable) {
    -1
}