package com.appetiser.commons.droid.ui

import android.view.View
import com.google.android.material.textfield.TextInputLayout

fun TextInputLayout.setErrorMessage(message: String?) {
    error = message
    isErrorEnabled = message != null
}

var View.isDisplayed
    get() = visibility == View.VISIBLE
    set(value) {
        visibility = if (value) View.VISIBLE else View.GONE
    }