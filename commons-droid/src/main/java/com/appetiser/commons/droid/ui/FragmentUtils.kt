package com.appetiser.commons.droid.ui

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment

fun Fragment.showHomeAsUpEnabled(isUpEnabled: Boolean) {
    (activity as? AppCompatActivity)?.supportActionBar?.setDisplayHomeAsUpEnabled(isUpEnabled)
}